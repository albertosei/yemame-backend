// Add jsonwebtoken
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const config = require('../config/env/index');
const User = require('../models/user');

const addUser = async (body) => {
    const { firstName, lastName, email, password } = body;
    const user = await User.findOne({ email });
    if(user){
        throw {
            code: 409,
            message: 'User already exists',
            data: null,
            status: 'error'
        }
    }

    const saltRounds = 12;
    const hash = bcrypt.hashSync(password, saltRounds);
    const response = new User({
        firstName,
        lastName,
        email,
        password: hash
    })
    await response.save();

    return {
        code: 201,
        status: 'success',
        message: 'Thank you for signing up',
        data: response
    }
}

const loginUser = async (body) => {
    const { email, password } = body

    const user = await User.findOne({ email })
    if(!user) {
        throw{
            status: 'error',
            message: 'Wrong email and password combination',
            code: 404,
            data: null
        }
    }

    const checkForMatch = bcrypt.compareSync(password, user.password);
    if(!checkForMatch) {
        throw {
            status: 'error',
            message: 'Invalid password input',
            code: 400,
            data: null
        }
    }

    const options = {
        expiresIn: '1d'
    };

    const token = jwt.sign({
        _id: user._id,
        email
    }, config.JWT_SECRET_KEY, options);
    return {
        status: 'success',
        message: 'Authentication successful',
        code: 200,
        data: {
            user,
            token
        }
    }
}

const getSingleUser = async (id) => {
    const user = await User.find({ _id: id }, { password: 0 });
    if(!user) {
        throw {
            status: 'error',
            message: 'User not found',
            code: 400,
            data: null
        };
    }
    return {
        status: 'success',
        message: 'User fetched successfully',
        code: 200,
        data: user
    }
}

const getAllUsers = async () => {
    const users = await User.find({}, { password: 0 });
    return {
        status: 'success',
        message: 'Users fetched successfully',
        code: 200,
        data: users

    }
}

module.exports = {
    addUser,
    loginUser,
    getSingleUser,
    getAllUsers
}