const Yem = require('../models/yem');

const addYem = async (body) => {
    const { title, description, image } = body;
    const yem = await Yem.findOne({ description });
    if (yem) {
        throw {
            status: "error",
            message: "Sorry, this yem has already been created",
            code: 409,
            data: null
        };
    }
    const response = new Yem({
        title,
        description,
        image
    });
    await response.save();

    return {
        status: "success",
        message: "Yem has been added successfully",
        code: 200,
        data: response
    }
}

const getAllYems = async () => {
    const yems = await Yem.find().populate({path: 'postedBy', model: 'User'});
    return {
        status: 'success',
        code: 200,
        message: 'All yems have been fetched successfully',
        data: yems
    }
}

const getOneYem = async (id) => {
    const yem = await Yem.find({ _id: id });
    if (!yem) {
        throw {
            status: 'error',
            message: 'Yem not found',
            code: 400, 
            data: null
        };
    }
    return {
        status: 'success',
        message: 'Yem fetched successfully',
        code: 200,
        data: yem
    };
};

/**
 * Updates the status of a yem
 * @param {number} id - yem id
 * @param {string} status - yem status
 * @return { Promise<Object | Error> } A promise that resolves or rejects with 
 * an object of the yem status
 */
const updateYemStatus = async (body, id) => {
    const { status } = body;
    const yem = await Yem.findById({ _id: id });
    if(yem) {
        const oneYem = await Yem.findOneAndUpdate({ _id: id }, { $set: { status } });
        return {
            status: 'success',
            message: 'Yem status updated successfully',
            code: 200,
            data: oneYem
        }
    }
    return {
        status: 'error',
        message: 'Yem does not exist',
        code: 400,
        data: null
    }
}

module.exports = {
    addYem,
    getAllYems,
    getOneYem,
    updateYemStatus
}
