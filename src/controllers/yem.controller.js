const YemService = require('../services/yem.service');

const createYem = async (req, res, next) => {
    try {
        const response = await YemService.addYem(req.body);
        return res.status(response.code).json(response)
    } catch (error) {
        next(error)
    }
}

const getAllYems = async (req, res, next) => {
    try {
        const response = await YemService.getAllYems();
        return res.status(response.code).json(response);
    } catch (error) {
        next(error)
    }
}

const getOneYem = async (req, res, next) => {
    const { id } = req.params;
    try {
        const response = await YemService.getOneYem(id);
        return res.status(response.code).json(response);
    } catch (error) {
        next(error);
    }
};

const updateYemStatus = async (req, res, next) => {
    const { id } = req.params;
    try {
        const response = await YemService.updateYemStatus(req.body, id);
        return res.status(response.code).json(response);
    } catch (error) {
        next(error)
    }
}

module.exports = {
    createYem,
    getAllYems,
    getOneYem,
    updateYemStatus
}