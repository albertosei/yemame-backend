const UserService = require('../services/user.service')

const addUser = async (req, res, next) => {
    try {
        const response = await UserService.addUser(req.body);
        return res.status(response.code).json(response)
    } catch (error) {
        next(error)
    }
}

const loginUser = async (req, res, next) => {
    try {
        const response = await UserService.loginUser(req.body);
        return res.status(response.code).json(response)
    } catch (error) {
        next(error)
    }
}

const getSingleUser = async (req, res, next) => {
    try {
        const { id } = req.params
        const response = await UserService.getSingleUser(id);
        return res.status(response.code).json(response)
    } catch (error) {
        next(error)
    }
}

const getAllUsers = async (req, res, next) => {
    try {
        const response = await UserService.getAllUsers();
        return res.status(response.code).json(response)
    } catch (error) {
      next(error)  
    }
}

module.exports = {
    addUser,
    loginUser,
    getSingleUser,
    getAllUsers
}