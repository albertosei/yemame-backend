const mongoose = require('mongoose');

let Schema = mongoose.Schema;

const yemSchema = new Schema({
    title: {type: String, required: true},
    description: {type: String, required: true},
    image: {type: String},
    status: { type: String,
        enum: ['open', 'closed'], 
        default: 'open'
    },
    postedBy: { type: Schema.Types.ObjectId, ref: 'User'},
    createdAt: { type: Date, default: Date.now }
})

module.exports = mongoose.model('Yem', yemSchema);