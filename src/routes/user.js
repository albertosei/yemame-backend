const express = require('express');
const { addUser, loginUser, getSingleUser, getAllUsers } = require('../controllers/user.controller')

const router = express.Router();

router.post('/signup', addUser);
router.post('/login', loginUser);
router.get('/', getAllUsers)
router.get('/:id', getSingleUser)


module.exports = router;
