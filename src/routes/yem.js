const express = require('express');
const { createYem, getAllYems, getOneYem, updateYemStatus } = require('../controllers/yem.controller');
const { verifyToken } = require('../middlewares/auth.middleware');

const router = express.Router();

router.post('/', verifyToken, createYem);
router.get('/', verifyToken, getAllYems);
router.get('/:id', verifyToken, getOneYem);
router.patch('/:id', verifyToken, updateYemStatus);

module.exports = router;