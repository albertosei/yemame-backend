const express = require('express')
const user = require('../../routes/user');
const yem = require('../../routes/yem');
const api = express.Router()

api.get('/', (req, res) => res.status(200).json({
    status: 'success',
    message: 'Welcome to Yemame API'
}))

api.use('/user', user)
api.use('/yem', yem)

module.exports = api