const mongoose = require('mongoose');
const config = require('./env/index');

const connectionString = config.DATABASE_URL;

// Connect with MongoDB
mongoose.connect(connectionString, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

// This is to check for successful connection
mongoose.connection.on('connected', function () {
    console.log("connected to mongo")
})
mongoose.connection.on('error', function () {
    console.log("error connecting")
})