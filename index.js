// Import database config
require('./src/config/database.config');

const cors = require('cors');
const express = require('express');
const apiVersion1 = require('./src/config/versioning/v1');
const { notFound, appErrorHandler, genericErrorHandler } = require('./src/middlewares/error.middleware');
const envConfig = require('./src/config/env/index');

const app = express();

app.use(express.json())

const PORT = envConfig.PORT || 3246;

app.listen(PORT, () => {
    console.log(`Application running on port ${PORT}`)
})

app.use(cors());
app.use('/api/v1', apiVersion1);
app.use(appErrorHandler);
app.use(genericErrorHandler);
app.use(notFound)

module.exports = app;

